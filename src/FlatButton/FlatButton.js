import React, { Component } from 'react';

type Props = {
  title: string;
}

class FlatButton extends Component {
  static euiName = 'FlatButton';
  constructor(props: Props) {
    super(props);
    this.props = props;
  }

  render() {
    const { title } = this.props;
    return (
      <button type="button">{ title }</button>
    );
  }
}

export default FlatButton;
